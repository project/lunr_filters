# Lunr exposed filters

- Unused values in the exposed filters are removed
- Warning: Filter identifier has to match field name
- Mimics facet summary
- Works with [Tome](https://www.drupal.org/project/tome)
- Needs [Lunr](https://www.drupal.org/project/lunr) and [Select2](https://www.drupal.org/project/select2)
- Supports table output
