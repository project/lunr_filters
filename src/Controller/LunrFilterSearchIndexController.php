<?php

namespace Drupal\lunr_filters\Controller;

use Drupal\lunr\Controller\LunrSearchIndexController;
use Drupal\lunr\LunrSearchInterface;

/**
 * Controller for Lunr search indexing.
 */
class LunrFilterSearchIndexController extends LunrSearchIndexController {

  /**
   * Provides a page to trigger the indexing process.
   *
   * @param \Drupal\lunr\LunrSearchInterface $lunr_search
   *   The Lunr search entity.
   *
   * @return array
   *   A render array.
   */
  public function page(LunrSearchInterface $lunr_search) {
    $build = parent::page($lunr_search);

    $view = $lunr_search->getView();

    if (!$view) {
      return $build;
    }

    $index_fields = [];
    $display_fields = [];
    $field_options = $view->getDisplay()->options['row']['options']['field_options'];
    foreach ($field_options as $field => $field_option) {
      // Add to index.
      if ($field_option['index']) {
        $index_fields[$field] = ['boost' => (int) $field_option['boost'] ?? 1];
      }

      // Add to display.
      if ($field_option['display']) {
        if (!empty($field_option['alias'])) {
          $display_fields[] = $field_option['alias'];
        }
        else {
          $display_fields[] = $field;
        }
      }
    }

    $build['#attached']['drupalSettings']['lunr']['indexSettings'][$lunr_search->id()]['indexFields'] = $index_fields;
    $build['#attached']['drupalSettings']['lunr']['indexSettings'][$lunr_search->id()]['displayField'] = implode(',', $display_fields);

    return $build;
  }

}
