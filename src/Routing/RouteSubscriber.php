<?php

namespace Drupal\lunr_filters\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.lunr_search.index')) {
      $route->setDefaults([
        '_controller' => '\Drupal\lunr_filters\Controller\LunrFilterSearchIndexController::page',
        '_title' => 'Index Lunr filter search',
      ]);
    }
  }

}
