<?php

namespace Drupal\lunr_filters\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lunr\Plugin\views\style\LunrSearchIndexJson;

/**
 * The style plugin for serialized output formats.
 *
 * This class is largely based on the core REST module.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "lunr_filters_search_index_json",
 *   title = @Translation("Lunrfilters search index JSON"),
 *   help = @Translation("Serializes views row data to JSON."),
 *   display_types = {"lunr_filters_search_index"}
 * )
 */
class LunrFiltersSearchIndexJson extends LunrSearchIndexJson {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['disable_search'] = [
      'default' => FALSE,
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['disable_search'] = [
      '#title' => $this->t('Disable free text search'),
      '#description' => $this->t('Disable free text search.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['disable_search'],
    ];
  }

}
