<?php

namespace Drupal\lunr_filters\Plugin\views\row;

use Drupal\lunr\Plugin\views\row\LunrSearchIndexRow;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\row\RowPluginBase;

/**
 * Plugin which displays fields as raw data.
 *
 * This class is largely based on the core REST module.
 *
 * @ingroup views_row_plugins
 *
 * @ViewsRow(
 *   id = "lunr_filters_search_index_row",
 *   title = @Translation("Lunr filters search index row"),
 *   help = @Translation("Use fields in index."),
 *   display_types = {"lunr_filters_search_index"}
 * )
 */
class LunrFiltersSearchIndexRow extends LunrSearchIndexRow {

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * Stores an array of prepared field aliases from options.
   *
   * @var array
   */
  protected $replacementAliases = [];

  /**
   * Stores an array of options to determine if the raw field output is used.
   *
   * @var array
   */
  protected $rawOutputOptions = [];

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    if (!empty($this->options['field_options'])) {
      $options = (array) $this->options['field_options'];
      // Prepare a trimmed version of replacement aliases.
      $aliases = static::extractFromOptionsArray('alias', $options);
      $this->replacementAliases = array_filter(array_map('trim', $aliases));
      // Prepare an array of raw output field options.
      $this->rawOutputOptions = static::extractFromOptionsArray('raw_output', $options);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['field_options'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    RowPluginBase::buildOptionsForm($form, $form_state);

    $form['field_options_info'] = [
      '#markup' => '<h4>Remarks</h4>
        <ul>
          <li>If both index and display is needed make sure to specify an alias.</li>
          <li>Indexed field will always use raw value</li>
          <li>Exposed filters will be rendered as dropdowns</li>
          <li>If you select multiple fields for display, it will output a table</li>
        </ul>
      ',
    ];

    $form['field_options'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Field'),
        $this->t('Index'),
        $this->t('Boost'),
        $this->t('Display'),
        $this->t('Alias'),
        $this->t('Raw output'),
      ],
      '#empty' => $this->t('You have no fields. Add some to your view.'),
      '#tree' => TRUE,
    ];

    $options = $this->options['field_options'];

    if ($fields = $this->view->display_handler->getOption('fields')) {
      foreach ($fields as $id => $field) {
        // Don't show the field if it has been excluded.
        if (!empty($field['exclude'])) {
          continue;
        }
        $form['field_options'][$id]['field'] = [
          '#markup' => $id,
        ];
        $form['field_options'][$id]['index'] = [
          '#title' => $this->t('Index @id', ['@id' => $id]),
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
          '#default_value' => isset($options[$id]['index']) ? $options[$id]['index'] : '',
        ];
        $form['field_options'][$id]['boost'] = [
          '#title' => $this->t('Boost @id', ['@id' => $id]),
          '#title_display' => 'invisible',
          '#type' => 'number',
          '#min' => 1,
          '#step' => 1,
          '#default_value' => isset($options[$id]['boost']) ? $options[$id]['boost'] : '1',
        ];
        $form['field_options'][$id]['display'] = [
          '#title' => $this->t('Display @id', ['@id' => $id]),
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
          '#default_value' => isset($options[$id]['display']) ? $options[$id]['display'] : '',
        ];
        $form['field_options'][$id]['alias'] = [
          '#title' => $this->t('Alias for @id', ['@id' => $id]),
          '#title_display' => 'invisible',
          '#type' => 'textfield',
          '#default_value' => isset($options[$id]['alias']) ? $options[$id]['alias'] : '',
          '#element_validate' => [[$this, 'validateAliasName']],
          '#states' => [
            'visible' => [
              ':input[name="row_options[field_options][' . $id .'][display]"]' => ['checked' => TRUE],
            ],
            'required' => [
              ':input[name="row_options[field_options][' . $id .'][display]"]' => ['checked' => TRUE],
            ],
          ],
        ];
        $form['field_options'][$id]['raw_output'] = [
          '#title' => $this->t('Raw output for @id', ['@id' => $id]),
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
          '#default_value' => isset($options[$id]['raw_output']) ? $options[$id]['raw_output'] : '',
        ];
      }
    }
  }

  /**
   * Form element validation handler.
   */
  public function validateAliasName($element, FormStateInterface $form_state) {
    if (preg_match('@[^A-Za-z0-9_-]+@', $element['#value'])) {
      $form_state->setError($element, $this->t('The machine-readable name must contain only letters, numbers, dashes and underscores.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    // Collect an array of aliases to validate.
    $aliases = static::extractFromOptionsArray('alias', $form_state->getValue(['row_options', 'field_options']));

    // If array filter returns empty, no values have been entered. Unique keys
    // should only be validated if we have some.
    if (($filtered = array_filter($aliases)) && (array_unique($filtered) !== $filtered)) {
      $form_state->setErrorByName('aliases', $this->t('All field aliases must be unique'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    /** @var \Drupal\views\ResultRow $row */
    $field_options = (array) $this->options['field_options'];
    $output = [];

    foreach ($this->view->field as $id => $field) {
      // Output fields needed for the index.
      if (!empty($field_options[$id]['index'])) {
        $value = $field->getValue($row);
        $output[$id] = $value;
      }

      // Output fields needed for the display.
      if (!empty($field_options[$id]['display'])) {
        // If the raw output option has been set, just get the raw value.
        if (!empty($this->rawOutputOptions[$id])) {
          $value = $field->getValue($row);
        }
        // Otherwise, pass this through the field advancedRender() method.
        else {
          $value = $field->advancedRender($row);
        }

        // Omit excluded fields from the rendered output.
        if (empty($field->options['exclude'])) {
          $output[$this->getFieldKeyAlias($id)] = $value;
        }
      }
    }

    $output['ref'] = ($this->view->getPager()->usePager() ? $this->view->getPager()->getCurrentPage() : 0) . ':' . $row->index;

    return $output;
  }

  /**
   * Return an alias for a field ID, as set in the options form.
   *
   * @param string $id
   *   The field id to lookup an alias for.
   *
   * @return string
   *   The matches user entered alias, or the original ID if nothing is found.
   */
  public function getFieldKeyAlias($id) {
    if (isset($this->replacementAliases[$id])) {
      return $this->replacementAliases[$id];
    }

    return $id;
  }

  /**
   * Extracts a set of option values from a nested options array.
   *
   * @param string $key
   *   The key to extract from each array item.
   * @param array $options
   *   The options array to return values from.
   *
   * @return array
   *   A regular one dimensional array of values.
   */
  protected static function extractFromOptionsArray($key, array $options) {
    return array_map(function ($item) use ($key) {
      return isset($item[$key]) ? $item[$key] : NULL;
    }, $options);
  }

}
