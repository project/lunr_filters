<?php

namespace Drupal\lunr_filters\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\tome_static\Event\CollectPathsEvent;
use Drupal\tome_static\Event\TomeStaticEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds index filenames for Tome exports.
 */
class TomePathSubscriber implements EventSubscriberInterface {

  /**
   * The Lunr search entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $lunrSearchStorage;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Path resolver
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $pathResolver;

  /**
   * Constructs the EntityPathSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, ExtensionPathResolver $path_resolver) {
    $this->lunrSearchStorage = $entity_type_manager->getStorage('lunr_search');
    $this->fileSystem = $file_system;
    $this->pathResolver = $path_resolver;
  }

  /**
   * Reacts to a collect paths event.
   *
   * @param \Drupal\tome_static\Event\CollectPathsEvent $event
   *   The collect paths event.
   */
  public function collectPaths(CollectPathsEvent $event) {
    /** @var \Drupal\lunr\LunrSearchInterface $search */
    foreach ($this->lunrSearchStorage->loadMultiple() as $search) {
      $directory = dirname($search->getBaseIndexPath());
      foreach (array_keys($this->fileSystem->scanDirectory($directory, '/.*/')) as $filename) {
        $event->addPath(\Drupal::service('file_url_generator')->generateAbsoluteString($filename), ['language_processed' => 'language_processed']);
      }
    }
    $event->addPath($this->pathResolver->getPath('module', 'lunr_filters') . '/js/search.worker.js', ['language_processed' => 'language_processed']);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[TomeStaticEvents::COLLECT_PATHS][] = ['collectPaths'];
    return $events;
  }

}
